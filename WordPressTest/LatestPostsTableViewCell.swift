//
//  LatestPostsTableViewCell.swift
//  WordPressTest
//
//  Created by Christopher Musante on 7/14/16.
//  Copyright © 2016 Christopher Musante. All rights reserved.
//

import UIKit

class LatestPostsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var postTitle: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var postImage: UIImageView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
